import React from 'react';
import LifeCycleWrapper from "./more/LifeCycle/LifeCycleWrapper";
import NullRenderWrapper from "./more/NullRender/NullRenderWrapper";
import FormWrapper from "./more/FileForm/FormWrapper";
import LiftStateWrapper from "./more/LiftState/LiftStateWrapper";

import './App.css';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            components: [
                {
                    value: <LifeCycleWrapper />,
                    show: false
                },
                {
                    value: <NullRenderWrapper />,
                    show: false
                },
                {
                    value: <FormWrapper />,
                    show: true
                },
                {
                    value: <LiftStateWrapper />,
                    show: true
                }
            ]
        };
    }

    getComponents() {
        return this.state.components
            .filter(component => component.show)
            .map((component, index, arr) => {
                return (
                    <div key={index} className="app-components">
                        {component.value}
                        {/*{index !== arr.length - 1 && <hr />}*/}
                    </div>
                );
            });
    }

    render() {
        return (
            <div className="app">
                {this.getComponents()}
            </div>
        );
    }
}

export default App;
