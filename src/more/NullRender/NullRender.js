import React from 'react';

export default class NullRender extends React.Component {
    componentDidMount() {
        console.log('componentDidMount');
    }

    componentWillUnmount() {
        console.log('componentWillUnmount');
    }

    render() {
        if (!this.props.show) {
            return null;
        }

        return (
            <div>hello there</div>
        );
    }
}
