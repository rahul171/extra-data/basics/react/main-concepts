import React from 'react';
import NullRender from "./NullRender";

export default class NullRenderWrapper extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            return: true,
            show: true
        };
    }

    toggleShow = () => {
        this.setState((state) => ({
            return: !state.return
        }));
    }

    toggle = () => {
        this.setState((state) => ({
            show: !state.show
        }));
    }

    render() {
        return (
            <div className="null-render-wrapper">
                <div>
                    <button
                        onClick={this.toggleShow}
                    >toggle return null</button>
                    <button
                        onClick={this.toggle}
                    >toggle</button>
                </div>
                <div className="null-renders">
                    {this.state.show &&
                        <NullRender show={this.state.return} />
                    }
                </div>
            </div>
        );
    }
}
