import React from 'react';

export default class LifeCycle extends React.Component {
    componentDidMount() {
        console.log('componentDidMount');
    }

    componentWillUnmount() {
        console.log('componentWillUnmount');
    }

    render() {
        return (
            <div className="lifecycle">
                lifecycle component
            </div>
        );
    }
}
