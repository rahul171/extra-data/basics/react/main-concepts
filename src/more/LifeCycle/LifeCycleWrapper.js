import React from 'react';
import LifeCycle from './LifeCycle';

export default class LifeCycleWrapper extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showLifeCycle: true
        };
    }

    toggleLifeCycle = () => {
        this.setState((state) => ({
            showLifeCycle: !state.showLifeCycle
        }));
    }

    render() {
        return (
            <div className="lifecycle-wrapper">
                <div>
                    <button
                        onClick={this.toggleLifeCycle}
                    >toggle</button>
                </div>
                <div>
                    {this.state.showLifeCycle &&
                        <LifeCycle />
                    }
                </div>
            </div>
        );
    }
}
