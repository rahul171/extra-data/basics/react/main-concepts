import React from 'react';

export default class Form extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            // try not setting default value here, just leave the state object empty, and see the error
            // in terminal when you type something in the input box.
            // to make the input element controlled, you must define the initial value.
            default: ''
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
    }

    handleReset = (e) => {
        this.setState({ default: '' });
    }

    logState = () => {
        console.log(this.state);
    }

    render() {
        return (
            <div className="form">
                <form onSubmit={this.handleSubmit}>
                    input: {this.state.default}<br />
                    no value = state<input type="text" name="default" onChange={this.handleChange} /><br />
                    value = state<input type="text" value={this.state.default} name="default" onChange={this.handleChange} /><br />
                    <input type="submit" value="submit" />
                    <button type="button" onClick={this.handleReset}>reset</button>
                    <button type="button" onClick={this.logState}>log state</button>
                </form>
            </div>
        );
    }
}
