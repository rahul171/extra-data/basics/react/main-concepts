import React from 'react';

export default class Form1 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            select: this.getDefaultSelected()
        }
    }

    getDefaultSelected() {
        return 'value1';
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
    }

    handleReset = (e) => {
        this.setState({ select: this.getDefaultSelected() });
    }

    logState = (e) => {
        console.log(this.state);
    }

    render() {
        return (
            <div className="form1">
                <form onSubmit={this.handleSubmit}>
                    <select value={this.state.select} name="select" onChange={this.handleChange}>
                        <option value="value1">value1</option>
                        <option value="value2">value2</option>
                        <option value="value3">value3</option>
                        <option value="value4">value4</option>
                    </select>
                    <button type="submit">submit</button>
                    <button type="button" onClick={this.handleReset}>reset</button>
                    <button type="button" onClick={this.logState}>log state</button>
                </form>
            </div>
        );
    }
}
