import React from 'react';
import Form from './Form';
import Form1 from './Form1';
import Form2 from './Form2';
import Form3 from './Form3';
import Form4 from './Form4';
import File1 from './File1';

class FormWrapper extends React.Component {
    render() {
        return (
            <div className="file-form">
                <Form /><hr />
                <Form1 /><hr />
                <Form2 /><hr />
                <Form3 /><hr />
                <Form4 /><hr />
                <File1 />
            </div>
        );
    }
}

export default FormWrapper;
