import React from 'react';

export default class Form4 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    render() {
        return (
            <div className="form4">
                {/* see the errors in the console */}
                <input type="text" value="hello" />
                <input type="text" defaultValue="hello" />
                <input type="text" value={null} />
                <input type="text" value={undefined} />
                <input type="text" value='' />
                <input type="text" />
                <input type="text" value='' readOnly="readOnly" />
                <br />
                {/*
                    Always use defaultChecked/defaultValue for uncontrolled (radio or checkbox)/(input, select, textarea etc)
                    when not used defaultChecked, etc for uncontrolled and simply used checked, etc,
                    it behaves a little weird when clicked on the radio button.
                    have to click 2 times and then it moves 1 step.
                */}
                <input type="radio" name="radioGroup" value="hello1" />
                <input type="radio" name="radioGroup" value="hello2" checked />
                <input type="radio" name="radioGroup" value="hello3" />
                <br />
                <input type="radio" name="radioGroup1" value="hello1" />
                <input type="radio" name="radioGroup1" value="hello2" defaultChecked="defaultChecked" />
                <input type="radio" name="radioGroup1" value="hello3" />
            </div>
        );
    }
}
