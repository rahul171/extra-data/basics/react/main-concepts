import React from 'react';

export default class File1 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {}

        this.file = React.createRef();
        this.fileMultiple = React.createRef();
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.file.current.files);
        console.log(this.fileMultiple.current.files);
    }

    render() {
        return (
            <div className="file1">
                <form onSubmit={this.handleSubmit}>
                    <input type="file" ref={this.file} /><br />
                    <input type="file" multiple={true} ref={this.fileMultiple} />
                    <input type="submit" value="submit" />
                </form>
            </div>
        );
    }
}
