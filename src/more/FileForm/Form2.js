import React from 'react';

export default class Form1 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            select: this.getDefaultSelected()
        }
    }

    getDefaultSelected() {
        return ['value1', 'value3'];
    }

    handleChange = (e) => {
        // console.log(this.target.options);

        // console.log(Array.isArray(e.target.options));
        // console.log(Array.isArray(e.target.selectedOptions));
        // console.log(e.target.options instanceof Array);
        // console.log(e.target.selectedOptions instanceof Array);

        // not array, but its an html collection. so array methods won't work.
        // use Array.from() to convert into array first.

        // console.log(e.target.options.map(options => options));
        // console.log(e.target.selectedOptions.map(options => options));

        // console.log(Array.from(e.target.options).filter(o => o.selected).map(o => o.value));

        this.setState({
            [e.target.name]: Array.from(e.target.selectedOptions).map(element => element.value)
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
    }

    handleReset = (e) => {
        this.setState({ select: this.getDefaultSelected() });
    }

    logState = (e) => {
        console.log(this.state);
    }

    render() {
        return (
            <div className="form1">
                <form onSubmit={this.handleSubmit}>
                    <select multiple={true} value={this.state.select} name="select" onChange={this.handleChange}>
                        <option value="value1">value1</option>
                        <option value="value2">value2</option>
                        <option value="value3">value3</option>
                        <option value="value4">value4</option>
                    </select>
                    <button type="submit">submit</button>
                    <button type="button" onClick={this.handleReset}>reset</button>
                    <button type="button" onClick={this.logState}>log state</button>
                </form>
            </div>
        );
    }
}
