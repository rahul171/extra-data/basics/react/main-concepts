import React from 'react';

export default class Form2 extends React.Component {
    constructor(props) {
        super(props);

        this.state = this.getDefaultSelected();
    }

    getDefaultSelected() {
        return {
            paragraph: ''
        };
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
    }

    handleReset = (e) => {
        this.setState(this.getDefaultSelected());
    }

    logState = (e) => {
        console.log(this.state);
    }

    render() {
        return (
            <div className="form1">
                <form onSubmit={this.handleSubmit}>
                    <textarea name="paragraph" value={this.state.paragraph} onChange={this.handleChange} />
                    <button type="submit">submit</button>
                    <button type="button" onClick={this.handleReset}>reset</button>
                    <button type="button" onClick={this.logState}>log state</button>
                </form>
            </div>
        );
    }
}
