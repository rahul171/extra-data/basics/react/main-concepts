import React from 'react';
import TemperatureInput from "./more/TemperatureInput";

export default class LiftState extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            scale: 'c',
            temperature: 30
        }
    }

    handleCelsiusChange = (celsius) => {
        this.setState({
            scale: 'c',
            temperature: celsius
        });
    }

    handleFahrenheitChange = (fahrenheit) => {
        this.setState({
            scale: 'f',
            temperature: fahrenheit
        });
    }

    render() {
        const temperature = this.state.temperature;
        const temp = {};
        temp.celsius = this.state.scale !== 'c' ? convertTemperature(temperature, toCelsius) : temperature;
        temp.fahrenheit = this.state.scale !== 'f' ? convertTemperature(temperature, toFahrenheit) : temperature;

        return (
            <div className="lift-state">
                <TemperatureInput
                    scale="c"
                    temperature={temp.celsius}
                    onTemperatureChange={this.handleCelsiusChange} />
                <TemperatureInput
                    scale="f"
                    temperature={temp.fahrenheit}
                    onTemperatureChange={this.handleFahrenheitChange} />
            </div>
        );
    }
}

const convertTemperature = (temperature, convert) => {
    let temp = parseFloat(temperature);
    if (Number.isNaN(temp)) {
        return '';
    }
    temp = convert(temp);
    temp = Math.round(temp * 1000) / 1000;
    return temp;
}

const toCelsius = (fahrenheit) => {
    return (fahrenheit - 32) * 5 / 9
}

const toFahrenheit = (celsius) => {
    return (celsius * 9 / 5) + 32;
}
