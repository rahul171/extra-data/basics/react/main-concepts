import React from 'react';
import LiftState from "./LiftState";

export default class LiftStateWrapper extends React.Component {
    render() {
        return (
            <div className="lift-state-wrapper">
                <LiftState />
            </div>
        );
    }
}
