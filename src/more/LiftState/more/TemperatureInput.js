import React from 'react';

const temperatureNames = {
    c: 'Celsius',
    f: 'Fahrenheit'
};

export default class TemperatureInput extends React.Component {

    handleChange = (e) => {
        this.props.onTemperatureChange(e.target.value);
    }

    render() {
        return (
            <div className="temperature-input">
                <fieldset>
                    <legend>Enter temperature ({temperatureNames[this.props.scale]})</legend>
                    <input type="number" value={this.props.temperature} onChange={this.handleChange} />
                </fieldset>
            </div>
        );
    }
}
